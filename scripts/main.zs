#debug
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.jei.JEI;
import mods.ic2.MetalFormer;
import mods.ic2.BlockCutter;
import mods.ic2.Compressor;
import crafttweaker.item.IItemCondition;
import mods.immersiveengineering.MetalPress;
import mods.ic2.Macerator;

recipes.remove(<mekanismgenerators:reactor:1>);

var x = <mekanism:basicblock:8>;
var p = <galacticraftplanets:item_basic_mars:3>;
var a = <mekanism:atomicalloy>;

var steelIngot = <thermalfoundation:material:160>;
var bronzeIngot = <thermalfoundation:material:163>;
var platinumIngot = <thermalfoundation:material:134>;
var leadIngot = <thermalfoundation:material:131>;
var copperIngot = <thermalfoundation:material:128>;

var dustSteel = <thermalfoundation:material:96>;
var dustIron = <thermalfoundation:material>;
var dustGold = <thermalfoundation:material:1>;
var dustCopper = <thermalfoundation:material:64>;
var dustTin = <thermalfoundation:material:65>;
var dustSilver = <thermalfoundation:material:66>;
var dustLead = <thermalfoundation:material:67>;
var dustOsmium = <mekanism:dust:2>;
var dustAluminum = <thermalfoundation:material:68>;
var dustNickel = <thermalfoundation:material:69>;
var dustIridium = <thermalfoundation:material:71>;
var dustPlatinum = <thermalfoundation:material:70>;
var dustUranium = <nuclearcraft:dust:4>;
var dustBronze = <thermalfoundation:material:99>;

var ironPlate = <thermalfoundation:material:32>;
var bronzePlate = <thermalfoundation:material:355>;
var tinPlate = <thermalfoundation:material:321>;
var steelPlate = <thermalfoundation:material:352>;
var goldPlate = <thermalfoundation:material:33>;

var hardClay = <minecraft:stained_hardened_clay>;
var blazePowder = <minecraft:blaze_powder>;

var itemSlag = <thermalfoundation:material:864>;

function removeFully(item as IItemStack) {
    recipes.remove(item);
    JEI.removeAndHide(item);
}



recipes.addShaped("mekanismReactorBlock", <mekanismgenerators:reactor:1> * 4, [
        [p, x, p],
	[x, a, x],
	[p, x, p],
]);


recipes.addShaped("glowstoneClay", <minecraft:glowstone> * 9, [
        [hardClay, hardClay, hardClay],
	[hardClay, blazePowder, hardClay],
	[hardClay, hardClay, hardClay],
]);


recipes.remove(<ic2:ingot>);
recipes.addShaped("composite_ingot",<ic2:ingot> *2, [
	[ironPlate,ironPlate,ironPlate],
	[bronzePlate,bronzePlate,bronzePlate],
	[tinPlate,tinPlate,tinPlate]
]);

/* icbm stuff */
removeFully(<icbmclassic:missile:24>); /* basic empty missle*/
removeFully(<icbmclassic:grenade:5>);
removeFully(<icbmclassic:grenade:6>);
removeFully(<icbmclassic:grenade:7>);
removeFully(<icbmclassic:grenade:2>);
removeFully(<icbmclassic:grenade:3>);
removeFully(<icbmclassic:grenade>);
removeFully(<icbmclassic:grenade:1>);
removeFully(<icbmclassic:grenade:4>);
removeFully(<icbmclassic:bombcart>);
removeFully(<icbmclassic:bombcart:5>);
removeFully(<icbmclassic:bombcart:6>);
removeFully(<icbmclassic:bombcart:7>);
removeFully(<icbmclassic:bombcart:10>);
removeFully(<icbmclassic:bombcart:22>);
removeFully(<icbmclassic:bombcart:2>);
removeFully(<icbmclassic:bombcart:23>);
removeFully(<icbmclassic:bombcart:8>);
removeFully(<icbmclassic:bombcart:11>);
removeFully(<icbmclassic:bombcart:13>);
removeFully(<icbmclassic:bombcart:17>);
removeFully(<icbmclassic:bombcart:1>);
removeFully(<icbmclassic:bombcart:20>);
removeFully(<icbmclassic:bombcart:16>);
removeFully(<icbmclassic:bombcart:18>);
removeFully(<icbmclassic:bombcart:9>);
removeFully(<icbmclassic:bombcart:3>);
removeFully(<icbmclassic:bombcart:15>);
removeFully(<icbmclassic:bombcart:19>);
removeFully(<icbmclassic:bombcart:4>);
removeFully(<icbmclassic:bombcart:21>);
removeFully(<icbmclassic:sulfurdust>);
removeFully(<icbmclassic:signaldisrupter>);
removeFully(<icbmclassic:tracker>);
removeFully(<icbmclassic:defuser>);
removeFully(<icbmclassic:explosives:5>);
removeFully(<icbmclassic:explosives:6>);
removeFully(<icbmclassic:explosives:7>);
removeFully(<icbmclassic:explosives:10>);
removeFully(<icbmclassic:explosives:22>);
removeFully(<icbmclassic:explosives:2>);
removeFully(<icbmclassic:explosives:23>);
removeFully(<icbmclassic:explosives:17>);
removeFully(<icbmclassic:explosives:1>);
removeFully(<icbmclassic:missile:5>);
removeFully(<icbmclassic:missile:6>);
removeFully(<icbmclassic:missile:7>);
removeFully(<icbmclassic:missile:10>);
removeFully(<icbmclassic:missile:22>);
removeFully(<icbmclassic:missile:2>);
removeFully(<icbmclassic:missile:23>);
removeFully(<icbmclassic:missile:8>);
removeFully(<icbmclassic:missile:11>);
removeFully(<icbmclassic:missile:13>);
removeFully(<icbmclassic:missile:17>);
removeFully(<icbmclassic:missile:1>);
removeFully(<icbmclassic:missile:20>);
removeFully(<icbmclassic:missile:16>);
removeFully(<icbmclassic:missile:18>);
removeFully(<icbmclassic:missile:9>);
removeFully(<icbmclassic:missile:3>);
recipes.remove(<icbmclassic:missile:15>);
removeFully(<icbmclassic:missile>);
removeFully(<icbmclassic:missile:19>);
removeFully(<icbmclassic:missile:4>);
removeFully(<icbmclassic:missile:21>);
removeFully(<icbmclassic:rocketlauncher>);
removeFully(<icbmclassic:reinforcedglass>);
removeFully(<icbmclassic:explosives:8>);
removeFully(<icbmclassic:explosives:11>);
removeFully(<icbmclassic:explosives:13>);
removeFully(<icbmclassic:explosives:20>);
removeFully(<icbmclassic:explosives:16>);
removeFully(<icbmclassic:explosives:18>);
removeFully(<icbmclassic:explosives:9>);
removeFully(<icbmclassic:explosives:3>);
removeFully(<icbmclassic:explosives:15>);
removeFully(<icbmclassic:explosives>);
removeFully(<icbmclassic:explosives:19>);
removeFully(<icbmclassic:explosives:4>);
removeFully(<icbmclassic:explosives:21>);
removeFully(<icbmclassic:ingot>);
removeFully(<icbmclassic:ingot:1>);
removeFully(<icbmclassic:plate>);
removeFully(<icbmclassic:plate:1>);
removeFully(<icbmclassic:clump>);
removeFully(<icbmclassic:wire>);
removeFully(<icbmclassic:wire:1>);
removeFully(<icbmclassic:saltpeter>);
removeFully(<icbmclassic:spikes>);
removeFully(<icbmclassic:spikes:2>);
removeFully(<icbmclassic:spikes:1>);

recipes.remove(<icbmclassic:circuit:2>);
recipes.remove(<icbmclassic:emptower>);
recipes.remove(<icbmclassic:launcherscreen:2>);

removeFully(<nuclearcraft:ingot_block:2>);
removeFully(<nuclearcraft:ingot_block:4>);
removeFully(<nuclearcraft:ingot:4>);
removeFully(<nuclearcraft:ingot:2>);
removeFully(<nuclearcraft:dust:2>);
removeFully(<nuclearcraft:dust:1>);

recipes.addShaped("eliteCircuit", <icbmclassic:circuit:2>, [
        [platinumIngot, <minecraft:quartz>, platinumIngot],
	[<ic2:plate:4>, <icbmclassic:circuit:1>, <ic2:plate:4>],
	[<minecraft:gold_ingot>, <minecraft:quartz>, <minecraft:gold_ingot>],
]);

recipes.addShaped("emptower", <icbmclassic:emptower>, [
        [steelPlate, <immersiveengineering:metal_decoration1:1>, steelPlate],
	[<icbmclassic:battery>, <ore:circuitElite>, <icbmclassic:battery>],
	[steelPlate, <galacticraftcore:dishbase>, steelPlate],
]);

recipes.addShaped("launcherscreen3", <icbmclassic:launcherscreen:2>, [
        [steelIngot, <icbmclassic:launcherscreen:1>, steelIngot],
	[steelIngot, <ore:circuitElite>, steelIngot],
	[steelIngot, <galacticraftplanets:mars_machine:8>, steelIngot],
]);

recipes.addShaped("missle1", <icbmclassic:missile:24>, [
        [null, <galacticraftcore:nose_cone>, null],
	[p, <icbmclassic:circuit:2>, p],
	[p, <galacticraftcore:engine>, p],
]);

mods.mekanism.GasConversion.register(<nuclearcraft:fluid_sulfuric_acid>, <gas:sulfuricacid> * 10);
mods.mekanism.GasConversion.register(<liquid:sulfuric_acid>, <gas:sulfuricacid> * 10);


mods.qmd.target_chamber.addRecipe(<draconicevolution:energy_crystal:2>, ((<particle:proton>*1000)^5000)~2.0, <draconicevolution:chaos_shard:1>, <particle:positron>*2, null, <particle:electron>*2, 10000, 0.05, 0.0, 0.001);
mods.qmd.target_chamber.addRecipe(<draconicevolution:nugget>, ((<particle:proton>*10)^0)~2.0, <draconicevolution:nugget:1>, null, null, <particle:electron>*2, 50000, 0.05, 0.0, 0.0005);
mods.qmd.target_chamber.addRecipe(<mekanism:compressedredstone>, ((<particle:proton>*500)^5000)~2.0, <xxlsolarpanels:lirium>, null, null, <particle:positron>, 20000, 0.05, 0.0, 0.0005);
mods.qmd.target_chamber.addRecipe(<minecraft:sand>, ((<particle:proton>*100)^1000)~2.0, <minecraft:glowstone>, null, null, <particle:positron>, 20000, 0.05, 0.0, 0.0005);

mods.qmd.target_chamber.addRecipe(<minecraft:redstone_block>, ((<particle:proton>*1000000)^10000)~2.0, <super_solar_panels:crafting:8>, null, null, <particle:positron>, 30000, 0.05, 0.0, 0.01);
mods.qmd.target_chamber.addRecipe(<thermalfoundation:material:33>, ((<particle:proton>*50000)^1000)~2.0, <advanced_solar_panels:crafting:2>, null, null, <particle:positron>, 20000, 0.05, 0.0, 0.0005);



recipes.remove(<super_solar_panels:crafting:9>);
mods.qmd.target_chamber.addRecipe(<super_solar_panels:crafting:10>, ((<particle:positron>*20000)^50000)~10.0, <super_solar_panels:crafting:9>, null, null, <particle:electron>, 150000, 0.05, 0.0, 0.0005);

recipes.remove(<super_solar_panels:machines:3>);
recipes.addShaped("singular1", <super_solar_panels:machines:3>, [
        [<super_solar_panels:crafting>, <super_solar_panels:crafting>, <super_solar_panels:crafting>],
	[<super_solar_panels:crafting>, <super_solar_panels:crafting:5>, <super_solar_panels:crafting>],
	[<xxlsolarpanels:machines:20>, <super_solar_panels:crafting>, <xxlsolarpanels:machines:20>],
]);


recipes.remove(<xxlsolarpanels:machines:19>);
recipes.addShaped("fantastic_pan", <xxlsolarpanels:machines:19>, [
        [<super_solar_panels:crafting>, <super_solar_panels:crafting:17>, <super_solar_panels:crafting>],
	[<super_solar_panels:crafting:17>, <xxlsolarpanels:advanced_solar_core>, <super_solar_panels:crafting:17>],
	[<super_solar_panels:machines:2>, <super_solar_panels:crafting:17>, <super_solar_panels:machines:2>],
]);

recipes.remove(<super_solar_panels:machines:2>);
recipes.addShaped("spectral", <super_solar_panels:machines:2>, [
        [<super_solar_panels:crafting:14>, <super_solar_panels:crafting>, <super_solar_panels:crafting:14>],
	[<super_solar_panels:crafting>, <xxlsolarpanels:advanced_solar_core>, <super_solar_panels:crafting>],
	[<xxlsolarpanels:machines:18>, <super_solar_panels:crafting>, <xxlsolarpanels:machines:18>],
]);

/*steel dust*/
mods.mekanism.crusher.removeRecipe(<mekanism:otherdust:1>);

mods.mekanism.crusher.addRecipe(steelIngot, dustSteel);
mods.mekanism.smelter.addRecipe(<thermalfoundation:material:96>,steelIngot);
mods.mekanism.infuser.removeRecipe(<mekanism:otherdust:1>);
mods.mekanism.infuser.addRecipe("CARBON",10,<mekanism:enrichediron>,<thermalfoundation:material:96>);

/* mekanism dust to thermal dust */
mods.mekanism.enrichment.removeRecipe(<mekanism:dirtydust>);
mods.mekanism.enrichment.removeRecipe(<mekanism:dirtydust:1>);

mods.mekanism.enrichment.removeRecipe(<mekanism:dirtydust:3>);
mods.mekanism.enrichment.removeRecipe(<mekanism:dirtydust:4>);
mods.mekanism.enrichment.removeRecipe(<mekanism:dirtydust:5>);
mods.mekanism.enrichment.removeRecipe(<mekanism:dirtydust:6>);


mods.mekanism.crusher.addRecipe(<xxlsolarpanels:lirium_ore>, <xxlsolarpanels:lirium>*2);
mods.enderio.SagMill.addRecipe([<xxlsolarpanels:lirium>*2], [100], <xxlsolarpanels:lirium_ore>);
mods.mekanism.enrichment.addRecipe(<mekanism:dirtydust>, dustIron);
mods.mekanism.enrichment.addRecipe(<mekanism:dirtydust:1>, dustGold);

mods.mekanism.enrichment.addRecipe(<mekanism:dirtydust:3>, dustCopper);
mods.mekanism.enrichment.addRecipe(<mekanism:dirtydust:4>, dustTin);
mods.mekanism.enrichment.addRecipe(<mekanism:dirtydust:5>, dustSilver);
mods.mekanism.enrichment.addRecipe(<mekanism:dirtydust:6>, dustLead);

removeFully(<immersiveengineering:metal:8>);
removeFully(<immersiveengineering:metal:28>);
removeFully(<mekanism:nugget:4>);
removeFully(<immersiveengineering:metal:8>);
removeFully(<immersiveengineering:metal:28>);
removeFully(<mekanism:nugget:4>);
MetalPress.removeRecipe(<immersiveengineering:metal:30>);
MetalPress.removeRecipe(<immersiveengineering:metal:38>);
MetalPress.removeRecipe(<immersiveengineering:metal:39>);
MetalPress.removeRecipe(<immersiveengineering:metal:40>);
MetalPress.removeRecipe(<immersiveengineering:metal:37>);
MetalPress.removeRecipe(<immersiveengineering:metal:36>);
MetalPress.removeRecipe(<immersiveengineering:metal:32>);
MetalPress.removeRecipe(<immersiveengineering:metal:33>);
MetalPress.removeRecipe(<immersiveengineering:metal:31>);
MetalPress.removeRecipe(<immersiveengineering:metal:34>);
MetalPress.removeRecipe(<ic2:plate>);
MetalPress.removeRecipe(<ic2:plate:1>);
MetalPress.removeRecipe(<ic2:plate:2>);
MetalPress.removeRecipe(<ic2:plate:3>);
MetalPress.removeRecipe(<ic2:plate:5>);
MetalPress.removeRecipe(<ic2:plate:7>);
MetalPress.removeRecipe(<ic2:plate:8>);

removeFully(<ic2:ingot:2>);
removeFully(<ic2:plate>);
removeFully(<ic2:plate:1>);
removeFully(<ic2:plate:2>);
removeFully(<ic2:plate:3>);
removeFully(<ic2:plate:5>);
removeFully(<ic2:plate:7>);
removeFully(<ic2:plate:8>);

removeFully(<immersiveengineering:metal:30>);
removeFully(<immersiveengineering:metal:38>);
removeFully(<immersiveengineering:metal:39>);
removeFully(<immersiveengineering:metal:40>);
removeFully(<immersiveengineering:metal:37>);
removeFully(<immersiveengineering:metal:36>);
removeFully(<immersiveengineering:metal:36>);
removeFully(<immersiveengineering:metal:32>);
removeFully(<immersiveengineering:metal:33>);
removeFully(<immersiveengineering:metal:31>);
removeFully(<immersiveengineering:metal:34>);





MetalPress.addRecipe(<thermalfoundation:material:32>, <minecraft:iron_ingot>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:33>, <minecraft:gold_ingot>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:320>, <ore:ingotCopper>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:321>, <ore:ingotTin>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:322>, <ore:ingotSilver>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:323>, <ore:ingotLead>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:324>, <ore:ingotAluminium>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:325>, <ore:ingotNickel>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:352>, <ore:ingotSteel>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:353>, <ore:ingotElectrum>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:355>, <ore:ingotBronze>,<immersiveengineering:mold>,2000,1);
MetalPress.addRecipe(<thermalfoundation:material:356>, <ore:ingotConstantan>,<immersiveengineering:mold>,2000,1);

recipes.addShapeless("plateIron",<thermalfoundation:material:32>,[<immersiveengineering:tool:0>.anyDamage(),<minecraft:iron_ingot>]);
recipes.addShapeless("plateGold",<thermalfoundation:material:33>,[<immersiveengineering:tool:0>.anyDamage(),<minecraft:gold_ingot>]);
recipes.addShapeless("platePlatinum",<thermalfoundation:material:326>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotPlatinum>]);
recipes.addShapeless("plateIridium",<thermalfoundation:material:327>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotIridium>]);
recipes.addShapeless("plateCopper",<thermalfoundation:material:320>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotCopper>]);
recipes.addShapeless("plateTin",<thermalfoundation:material:321>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotTin>]);
recipes.addShapeless("plateSilver",<thermalfoundation:material:322>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotSilver>]);
recipes.addShapeless("plateLead",<thermalfoundation:material:323>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotLead>]);
recipes.addShapeless("plateAluminum",<thermalfoundation:material:324>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotAluminum>]);
recipes.addShapeless("plateNickel",<thermalfoundation:material:325>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotNickel>]);
recipes.addShapeless("plateSteel",<thermalfoundation:material:352>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotSteel>]);
recipes.addShapeless("plateElectrum",<thermalfoundation:material:353>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotElectrum>]);
recipes.addShapeless("plateBronze",<thermalfoundation:material:355>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotBronze>]);
recipes.addShapeless("plateConstantan",<thermalfoundation:material:356>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotConstantan>]);
recipes.addShapeless("plateMithril",<thermalfoundation:material:328>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotMithril>]);
recipes.addShapeless("plateInvar",<thermalfoundation:material:354>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotInvar>]);
recipes.addShapeless("plateSignalum",<thermalfoundation:material:357>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotSignalum>]);
recipes.addShapeless("plateLumium",<thermalfoundation:material:358>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotLumium>]);
recipes.addShapeless("plateEnderium",<thermalfoundation:material:359>,[<immersiveengineering:tool:0>.anyDamage(),<ore:ingotEnderium>]);

recipes.addShapeless("r_dustIron",dustIron,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreIron>]);
recipes.addShapeless("r_dustGold",dustGold,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreGold>]);
recipes.addShapeless("r_dustPlatinum",dustPlatinum,[<immersiveengineering:tool:0>.anyDamage(),<ore:orePlatinum>]);
recipes.addShapeless("r_dustIridium",dustIridium,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreIridium>]);
recipes.addShapeless("r_dustCopper",dustCopper,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreCopper>]);
recipes.addShapeless("r_dustTin",dustTin,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreTin>]);
recipes.addShapeless("r_dustSilver",dustSilver,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreSilver>]);
recipes.addShapeless("r_dustLead",dustLead,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreLead>]);
recipes.addShapeless("r_dustAluminum",dustAluminum,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreAluminum>]);
recipes.addShapeless("r_dustNickel",dustNickel,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreNickel>]);
recipes.addShapeless("r_dustOsmium",dustOsmium,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreOsmium>]);
recipes.addShapeless("r_dustUranium",dustUranium,[<immersiveengineering:tool:0>.anyDamage(),<ore:oreUranium>]);

recipes.addShapeless("r2_dustIron",dustIron,[<ic2:forge_hammer>.anyDamage(),<ore:oreIron>]);
recipes.addShapeless("r2_dustGold",dustGold,[<ic2:forge_hammer>.anyDamage(),<ore:oreGold>]);
recipes.addShapeless("r2_dustPlatinum",dustPlatinum,[<ic2:forge_hammer>.anyDamage(),<ore:orePlatinum>]);
recipes.addShapeless("r2_dustIridium",dustIridium,[<ic2:forge_hammer>.anyDamage(),<ore:oreIridium>]);
recipes.addShapeless("r2_dustCopper",dustCopper,[<ic2:forge_hammer>.anyDamage(),<ore:oreCopper>]);
recipes.addShapeless("r2_dustTin",dustTin,[<ic2:forge_hammer>.anyDamage(),<ore:oreTin>]);
recipes.addShapeless("r2_dustSilver",dustSilver,[<ic2:forge_hammer>.anyDamage(),<ore:oreSilver>]);
recipes.addShapeless("r2_dustLead",dustLead,[<ic2:forge_hammer>.anyDamage(),<ore:oreLead>]);
recipes.addShapeless("r2_dustAluminum",dustAluminum,[<ic2:forge_hammer>.anyDamage(),<ore:oreAluminum>]);
recipes.addShapeless("r2_dustNickel",dustNickel,[<ic2:forge_hammer>.anyDamage(),<ore:oreNickel>]);
recipes.addShapeless("r2_dustOsmium",dustOsmium,[<ic2:forge_hammer>.anyDamage(),<ore:oreOsmium>]);
recipes.addShapeless("r2_dustUranium",dustUranium,[<ic2:forge_hammer>.anyDamage(),<ore:oreUranium>]);

recipes.addShapeless("plateIron1",<thermalfoundation:material:32>,[<ic2:forge_hammer>.anyDamage(),<minecraft:iron_ingot>]);
recipes.addShapeless("plateGold1",<thermalfoundation:material:33>,[<ic2:forge_hammer>.anyDamage(),<minecraft:gold_ingot>]);
recipes.addShapeless("platePlatinum1",<thermalfoundation:material:326>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotPlatinum>]);
recipes.addShapeless("plateIridium1",<thermalfoundation:material:327>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotIridium>]);
recipes.addShapeless("plateCopper1",<thermalfoundation:material:320>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotCopper>]);
recipes.addShapeless("plateTin1",<thermalfoundation:material:321>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotTin>]);
recipes.addShapeless("plateSilver1",<thermalfoundation:material:322>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotSilver>]);
recipes.addShapeless("plateLead1",<thermalfoundation:material:323>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotLead>]);
recipes.addShapeless("plateAluminum1",<thermalfoundation:material:324>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotAluminum>]);
recipes.addShapeless("plateNickel1",<thermalfoundation:material:325>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotNickel>]);
recipes.addShapeless("plateSteel1",<thermalfoundation:material:352>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotSteel>]);
recipes.addShapeless("plateElectrum1",<thermalfoundation:material:353>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotElectrum>]);
recipes.addShapeless("plateBronze1",<thermalfoundation:material:355>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotBronze>]);
recipes.addShapeless("plateConstantan1",<thermalfoundation:material:356>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotConstantan>]);
recipes.addShapeless("plateMithril1",<thermalfoundation:material:328>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotMithril>]);
recipes.addShapeless("plateInvar1",<thermalfoundation:material:354>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotInvar>]);
recipes.addShapeless("plateSignalum1",<thermalfoundation:material:357>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotSignalum>]);
recipes.addShapeless("plateLumium1",<thermalfoundation:material:358>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotLumium>]);
recipes.addShapeless("plateEnderium1",<thermalfoundation:material:359>,[<ic2:forge_hammer>.anyDamage(),<ore:ingotEnderium>]);


MetalFormer.addRollingRecipe(<thermalfoundation:material:32>,<minecraft:iron_ingot>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:33>,<minecraft:gold_ingot>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:326>,<ore:ingotPlatinum>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:327>,<ore:ingotIridium>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:320>,<ore:ingotCopper>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:321>,<ore:ingotTin>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:322>,<ore:ingotSilver>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:323>,<ore:ingotLead>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:324>,<ore:ingotAluminum>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:325>,<ore:ingotNickel>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:352>,<ore:ingotSteel>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:353>,<ore:ingotElectrum>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:355>,<ore:ingotBronze>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:356>,<ore:ingotConstantan>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:328>,<ore:ingotMithril>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:354>,<ore:ingotInvar>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:357>,<ore:ingotSignalum>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:358>,<ore:ingotLumium>);
MetalFormer.addRollingRecipe(<thermalfoundation:material:359>,<ore:ingotEnderium>);


BlockCutter.addRecipe(<thermalfoundation:material:32> * 9, <minecraft:iron_block>,5);
BlockCutter.addRecipe(<thermalfoundation:material:33> * 9,<minecraft:gold_block>,5);
BlockCutter.addRecipe(<thermalfoundation:material:326> * 9,<ore:blockPlatinum>,8);
BlockCutter.addRecipe(<thermalfoundation:material:327> * 9,<ore:blockIridium>,8);
BlockCutter.addRecipe(<thermalfoundation:material:320> * 9,<ore:blockCopper>,5);
BlockCutter.addRecipe(<thermalfoundation:material:321> * 9,<ore:blockTin>,5);
BlockCutter.addRecipe(<thermalfoundation:material:322> * 9,<ore:blockSilver>,8);
BlockCutter.addRecipe(<thermalfoundation:material:323> * 9,<ore:blockLead>,5);
BlockCutter.addRecipe(<thermalfoundation:material:324> * 9,<ore:blockAluminum>,5);
BlockCutter.addRecipe(<thermalfoundation:material:325> * 9,<ore:blockNickel>,8);
BlockCutter.addRecipe(<thermalfoundation:material:352> * 9,<ore:blockSteel>,8);
BlockCutter.addRecipe(<thermalfoundation:material:353> * 9,<ore:blockElectrum>,8);
BlockCutter.addRecipe(<thermalfoundation:material:355> * 9,<ore:blockBronze>,5);
BlockCutter.addRecipe(<thermalfoundation:material:356> * 9,<ore:blockConstantan>,8);
BlockCutter.addRecipe(<thermalfoundation:material:328> * 9,<ore:blockMithril>,8);
BlockCutter.addRecipe(<thermalfoundation:material:354> * 9,<ore:blockInvar>,8);
BlockCutter.addRecipe(<thermalfoundation:material:357> * 9,<ore:blockSignalum>,8);
BlockCutter.addRecipe(<thermalfoundation:material:358> * 9,<ore:blockLumium>,8);
BlockCutter.addRecipe(<thermalfoundation:material:359> * 9,<ore:blockEnderium>,8);

/*
ingots
*/
furnace.remove(<mekanism:ingot:4>);
furnace.remove(<ic2:ingot:5>);
furnace.remove(<nuclearcraft:alloy:5>);
furnace.remove(<immersiveengineering:metal:8>);

recipes.remove(<ic2:ingot:5>);
recipes.remove(<immersiveengineering:metal:8>);
recipes.remove(<mekanism:nugget:4>);
recipes.remove(<immersiveengineering:metal:28>);

recipes.remove(<ic2:ingot:2>);
recipes.remove(<nuclearcraft:ingot>);
recipes.remove(<mekanism:nugget:5>);
recipes.remove(<immersiveengineering:metal:20>);
recipes.remove(<immersiveengineering:metal>);
recipes.remove(<galacticraftcore:basic_item:3>);

recipes.remove(<galacticraftcore:basic_item:4>);
recipes.remove(<mekanism:ingot:6>);
recipes.remove(<ic2:ingot:6>);
recipes.remove(<nuclearcraft:ingot:1>);
recipes.remove(<mekanism:nugget:6>);
recipes.remove(<ic2:ingot:1>);
recipes.remove(<ic2:resource:5>);
recipes.remove(<mekanism:ingot:2>);
recipes.remove(<nuclearcraft:alloy>);
recipes.remove(<mekanism:basicblock:1>);
recipes.remove(<mekanism:nugget:2>);
recipes.remove(<nuclearcraft:alloy>);


/*steel*/

removeFully(<mekanismtools:steelpickaxe>);
removeFully(<mekanismtools:steelaxe>);
removeFully(<mekanismtools:steelshovel>);
removeFully(<mekanismtools:steelhoe>);
removeFully(<mekanismtools:steelsword>);
removeFully(<galacticraftcore:steel_pickaxe>);
removeFully(<galacticraftcore:steel_axe>);
removeFully(<galacticraftcore:steel_hoe>);
removeFully(<galacticraftcore:steel_shovel>);
removeFully(<galacticraftcore:steel_sword>);

removeFully(<immersiveengineering:pickaxe_steel>);
removeFully(<immersiveengineering:axe_steel>);
removeFully(<immersiveengineering:sword_steel>);
removeFully(<immersiveengineering:shovel_steel>);
removeFully(<immersiveengineering:steel_armor_feet>);
removeFully(<immersiveengineering:steel_armor_legs>);
removeFully(<immersiveengineering:steel_armor_chest>);
removeFully(<immersiveengineering:steel_armor_head>);

removeFully(<mekanismtools:bronzeaxe>);
removeFully(<mekanismtools:bronzepickaxe>);
removeFully(<mekanismtools:bronzeshovel>);
removeFully(<mekanismtools:bronzehoe>);
removeFully(<mekanismtools:bronzesword>);
removeFully(<ic2:bronze_axe>);
removeFully(<ic2:bronze_hoe>);
removeFully(<ic2:bronze_pickaxe>);
removeFully(<ic2:bronze_shovel>);
removeFully(<ic2:bronze_sword>);








/*silver*/
recipes.remove(<immersiveengineering:storage:3>);
recipes.remove(<ic2:resource:15>);
recipes.remove(<nuclearcraft:ingot_block:13>);
recipes.remove(<ic2:ingot:4>);
recipes.remove(<projectred-core:resource_item:102>);
recipes.remove(<immersiveengineering:metal:23>);

/* bronze */
recipes.remove(<ic2:bronze_boots>);
recipes.remove(<ic2:bronze_chestplate>);
recipes.remove(<ic2:bronze_helmet>);
recipes.remove(<ic2:bronze_leggings>);

JEI.removeAndHide(<ic2:bronze_boots>);
JEI.removeAndHide(<ic2:bronze_chestplate>);
JEI.removeAndHide(<ic2:bronze_helmet>);
JEI.removeAndHide(<ic2:bronze_leggings>);

/* mutant beast */
removeFully(<mutantbeasts:endersoul_hand>);
removeFully(<mutantbeasts:creeper_shard>);
removeFully(<mutantbeasts:chemical_x>);
removeFully(<minecraft:spawn_egg>.withTag({EntityTag: {id: "mutantbeasts:spider_pig"}}));
removeFully(<minecraft:spawn_egg>.withTag({EntityTag: {id: "mutantbeasts:mutant_snow_golem"}}));
removeFully(<minecraft:spawn_egg>.withTag({EntityTag: {id: "mutantbeasts:mutant_enderman"}}));
removeFully(<minecraft:spawn_egg>.withTag({EntityTag: {id: "mutantbeasts:mutant_creeper"}}));
removeFully(<minecraft:spawn_egg>.withTag({EntityTag: {id: "mutantbeasts:creeper_minion"}}));
removeFully(<mutantbeasts:creeper_minion_tracker>);
removeFully(<mutantbeasts:hulk_hammer>);

/* ic2 drill */
removeFully(<ic2:te:60>);
recipes.remove(<ic2:te:57>);
recipes.addShaped("ic2_drill", <ic2:te:57>, [
        [null, <ore:chest>, null],
	[<ore:circuitBasic>, <ore:machineBlock>, <ore:circuitBasic>],
	[null, <ic2:mining_pipe>, null],
]);
recipes.remove(<ic2:scanner>);
recipes.remove(<ic2:scanner:26>);

recipes.remove(<ic2:advanced_scanner>);
recipes.remove(<ic2:advanced_scanner:26>);

recipes.addShaped("ic2_scanner", <ic2:scanner>, [
        [null, <ore:dustGlowstone>, null],
	[<ore:circuitBasic>, <minecraft:compass>, <ore:circuitBasic>],
	[null, <ore:itemCopperCable>, null],
]);

recipes.addShaped("ic2_adv_scanner", <ic2:advanced_scanner>, [
        [null, <minecraft:ender_pearl>, null],
	[<ore:circuitElite>, <ic2:scanner>, <ore:circuitElite>],
	[null, <ore:itemGoldCable>, null],
]); 

/* solar panels hard mode */
/*recipes.remove(<advanced_solar_panels:crafting:4>);*/
mods.nuclearcraft.FissionIrradiator.addRecipe(<advanced_solar_panels:crafting>, <advanced_solar_panels:crafting:4>, 228228, 0.0, 0.0, 0.0001);

/* glowing mushroom */
/* mods.qmd.irradiator.removeWithOutput(<nuclearcraft:glowing_mushroom>); */
/*mods.qmd.irradiator.addRecipe(<minecraft:brown_mushroom>, <nuclearcraft:glowing_mushroom>, {1, 1, 1});*/
 /* mods.qmd.irradiator.addRecipe(<minecraft:red_mushroom>, <nuclearcraft:glowing_mushroom>, {1, 1, 1}); */
/*mods.qmd.irradiator.addRecipe(<advanced_solar_panels:crafting>, <advanced_solar_panels:crafting:4>, 0.1, 0.1, 0.0001);*/

/*copper*/
removeFully(<nuclearcraft:dust>);
removeFully(<mekanism:dust:3>);
removeFully(<galacticraftcore:basic_block_core:5>);
removeFully(<galacticraftcore:basic_block_moon>);
removeFully(<galacticraftplanets:venus:7>);
removeFully(<immersiveengineering:ore>);
removeFully(<ic2:resource:1>);
removeFully(<mekanism:oreblock:1>);
removeFully(<nuclearcraft:ore>);
removeFully(<mekanism:nugget:5>);
removeFully(<immersiveengineering:metal:20>);
removeFully(<immersiveengineering:metal:9>);
removeFully(<enderio:item_material:26>);
removeFully(<immersiveengineering:metal>);

recipes.remove(<immersiveengineering:metal:20>);
recipes.remove(<mekanism:nugget:5>);
recipes.remove(<ic2:ingot:2>);
recipes.remove(<immersiveengineering:metal>);
recipes.remove(<immersiveengineering:storage>);



/* lead */
removeFully(<ic2:ingot:3>);
removeFully(<ic2:dust:10>);
removeFully(<ic2:resource:7>);
removeFully(<mekanism:dust:6>);
removeFully(<immersiveengineering:metal:11>);
removeFully(<galacticraftplanets:venus:12>);
removeFully(<galacticraftplanets:basic_item_venus:1>);
removeFully(<immersiveengineering:storage:2>);
removeFully(<immersiveengineering:metal:2>);
removeFully(<immersiveengineering:metal:22>);

recipes.addShapeless("dust_lead",<thermalfoundation:material:67>,[
        <ic2:dust:23>, <ic2:dust:23>, <ic2:dust:23>,
	<ic2:dust:23>, <ic2:dust:23>, <ic2:dust:23>,
	<ic2:dust:23>, <ic2:dust:23>, <ic2:dust:23>,
]);

recipes.addShapeless("dust_lead1",<thermalfoundation:material:67>*2,[
        <ore:dustPetrotheum>,<ore:oreLead>
]);
recipes.addShapeless("dust_lead2",<thermalfoundation:material:67>,[
        <ore:dustPetrotheum>,<ore:ingotLead>
]);

Compressor.addRecipe(<thermalfoundation:material:67>, <ic2:dust:23> * 9);

/* tin */
recipes.remove(<galacticraftplanets:venus:12>);
recipes.remove(<immersiveengineering:storage:2>);
recipes.remove(<immersiveengineering:metal:22>);
recipes.remove(<immersiveengineering:metal:2>);
recipes.remove(<ic2:ingot:3>);
recipes.remove(<galacticraftplanets:basic_item_venus:1>);
recipes.remove(<nuclearcraft:ingot:2>);
recipes.remove(<nuclearcraft:ingot_block:2>);

/* aluminum */
recipes.remove(<galacticraftcore:basic_item:5>);
recipes.remove(<nuclearcraft:ingot:12>);
recipes.remove(<immersiveengineering:storage:1>);
recipes.remove(<nuclearcraft:ingot_block:12>);
recipes.remove(<galacticraftcore:basic_block_core:11>);
recipes.remove(<immersiveengineering:metal:21>);

furnace.remove(<ic2:ingot:2>);
furnace.remove(<nuclearcraft:ingot>);
furnace.remove(<mekanism:ingot:5>);
furnace.remove(<immersiveengineering:metal>);
furnace.remove(<galacticraftcore:basic_item:3>);
furnace.remove(<projectred-core:resource_item:100>);


furnace.remove(<ic2:ingot:6>);
furnace.remove(<nuclearcraft:ingot:1>);
furnace.remove(<projectred-core:resource_item:101>);
furnace.remove(<galacticraftcore:basic_item:4>);
furnace.remove(<mekanism:ingot:6>);

JEI.removeAndHide(<immersiveengineering:storage>);
JEI.removeAndHide(<mekanism:ingot:5>);
JEI.removeAndHide(<projectred-core:resource_item:101>);
JEI.removeAndHide(<projectred-core:resource_item:100>);
JEI.removeAndHide(<mekanism:ingot:4>);
JEI.removeAndHide(<nuclearcraft:alloy:5>);
JEI.removeAndHide(<nuclearcraft:alloy>);
JEI.removeAndHide(<nuclearcraft:ingot>);
JEI.removeAndHide(<galacticraftcore:basic_item:3>);
JEI.removeAndHide(<galacticraftcore:basic_item:4>);
JEI.removeAndHide(<mekanism:ingot:6>);
JEI.removeAndHide(<nuclearcraft:ingot:1>);
JEI.removeAndHide(<mekanism:nugget:6>);

JEI.removeAndHide(<ic2:resource:5>);
JEI.removeAndHide(<mekanism:basicblock:1>);
JEI.removeAndHide(<mekanismtools:bronzehelmet>);
JEI.removeAndHide(<mekanismtools:bronzechestplate>);
JEI.removeAndHide(<mekanismtools:bronzeboots>);
JEI.removeAndHide(<mekanismtools:bronzeleggings>);
JEI.removeAndHide(<mekanismtools:steelhelmet>);
JEI.removeAndHide(<mekanismtools:steelchestplate>);
JEI.removeAndHide(<mekanismtools:steelboots>);
JEI.removeAndHide(<mekanismtools:steelleggings>);
JEI.removeAndHide(<galacticraftcore:steel_boots>);
JEI.removeAndHide(<galacticraftcore:steel_leggings>);
JEI.removeAndHide(<galacticraftcore:steel_chestplate>);
JEI.removeAndHide(<galacticraftcore:steel_helmet>);


furnace.addRecipe(steelIngot,<thermalfoundation:material:96>);
furnace.addRecipe(<thermalfoundation:material:129>,<ic2:dust:17>);
furnace.addRecipe(<thermalfoundation:material:128>,<ic2:dust:4>);

furnace.remove(<ic2:ingot:5>);
removeFully(<ic2:ingot:5>);

/* nuclear missle */

recipes.addShaped("missle_u235", <icbmclassic:missile:15>, [
        [null, <icbmclassic:missile:24>, null],
	[null, <trinity:bomb_u235>, null],
	[null, <galacticraftcore:fuel_canister_partial:1>, null],
]);

mods.enderio.AlloySmelter.removeRecipe(<ic2:ingot:5>);
mods.enderio.AlloySmelter.addRecipe(steelIngot, [<minecraft:iron_ingot>, <ore:dustCoal> * 4],10000, 1);
mods.enderio.AlloySmelter.addRecipe(steelIngot, [<minecraft:iron_ingot>, <ore:fuelCoke>], 5000, 1);

mods.immersiveengineering.Crusher.removeRecipe(<immersiveengineering:metal:17>);

/* uran */
mods.immersiveengineering.Crusher.removeRecipe(<immersiveengineering:metal:14>);
mods.immersiveengineering.Crusher.addRecipe(dustUranium, <ore:uraniumIngot>, 512);
mods.immersiveengineering.Crusher.addRecipe(dustUranium*2, <ore:oreUranium>, 512);
removeFully(<ic2:resource:10>);
removeFully(<advanced_solar_panels:crafting:11>);
removeFully(<reactor_stuff:uranium_dust>);
removeFully(<reactor_stuff:uranium_ingot>);
recipes.remove(<immersiveengineering:metal:14>);
mods.ic2.Macerator.addRecipe(dustUranium, <ore:uraniumIngot>);

removeFully(<immersiveengineering:metal:17>);
mods.immersiveengineering.Crusher.addRecipe(<thermalfoundation:material:96>, steelIngot, 512);


mods.immersiveengineering.ArcFurnace.removeRecipe(<immersiveengineering:metal:8>);

mods.immersiveengineering.ArcFurnace.addRecipe(steelIngot, <minecraft:iron_ingot>, itemSlag, 50, 512, [<immersiveengineering:material:17>]);
mods.immersiveengineering.ArcFurnace.addRecipe(steelIngot, <minecraft:iron_ingot>, itemSlag, 100, 1024, [<thermalfoundation:material:768> * 2]);


mods.immersiveengineering.BlastFurnace.removeRecipe(<immersiveengineering:metal:8>);
mods.immersiveengineering.BlastFurnace.addRecipe(steelIngot, <minecraft:iron_ingot>, 2000, itemSlag);

mods.ic2.BlastFurnace.addRecipe([steelIngot, itemSlag], <minecraft:iron_ore>, 1, 1000);
mods.ic2.BlastFurnace.addRecipe([steelIngot, itemSlag], <ore:crushedIron>, 1, 700);
mods.ic2.BlastFurnace.addRecipe([steelIngot, itemSlag], <minecraft:iron_ingot>, 1, 700);
mods.ic2.BlastFurnace.addRecipe([steelIngot, itemSlag], <ore:dustIron>, 1, 700);
mods.ic2.BlastFurnace.addRecipe([steelIngot, itemSlag], <ore:crushedPurifiedIron>, 1, 500);
Compressor.addRecipe(<ic2:misc_resource:1>, <thermalfoundation:material:199> * 9);

/* solar */
var elitePanel = <nuclearcraft:solar_panel_elite>;
var quantCore = <advanced_solar_panels:crafting:13>;
var platePlatinum = <thermalfoundation:material:326>;
recipes.addShaped("quantPanel", <advanced_solar_panels:machines:5>, [
        [null, quantCore, null],
	[platePlatinum, elitePanel, platePlatinum],
	[platePlatinum, platePlatinum, platePlatinum],
]);

/*
machines
*/

/*
blocks
*/
JEI.removeAndHide(<mekanism:basicblock:5>);
JEI.removeAndHide(<immersiveengineering:storage:8>);
JEI.removeAndHide(<ic2:resource:8>);

JEI.removeAndHide(<ic2:resource:6>);
JEI.removeAndHide(<galacticraftcore:basic_block_core:9>);
JEI.removeAndHide(<mekanism:basicblock:12>);
JEI.removeAndHide(<nuclearcraft:ingot_block>);

JEI.removeAndHide(<ic2:resource:9>);
JEI.removeAndHide(<galacticraftcore:basic_block_core:10>);
JEI.removeAndHide(<mekanism:basicblock:13>);
JEI.removeAndHide(<nuclearcraft:ingot_block:1>);

/*
 gears
*/
JEI.removeAndHide(<appliedenergistics2:material:40>);
JEI.removeAndHide(<enderio:item_material:10>);
JEI.removeAndHide(<enderio:item_material:9>);
